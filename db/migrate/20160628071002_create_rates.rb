class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.string :location1
      t.string :location2
      t.string :price

      t.timestamps null: false
    end
  end
end
