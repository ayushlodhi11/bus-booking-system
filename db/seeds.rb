# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



r = Rate.find_or_initialize_by({:location1 => "Chennai", :location2 => "Mumbai"})
r.price = "$150"
r.save!

r = Rate.find_or_initialize_by({:location1 => "Chennai", :location2 => "New Delhi"})
r.price = "$250"
r.save!

r = Rate.find_or_initialize_by({:location1 => "New Delhi", :location2 => "Mumbai"})
r.price = "$100"
r.save!


r = Rate.find_or_initialize_by({:location2 => "Chennai", :location1 => "Mumbai"})
r.price = "$150"
r.save!

r = Rate.find_or_initialize_by({:location2 => "Chennai", :location1 => "New Delhi"})
r.price = "$250"
r.save!

r = Rate.find_or_initialize_by({:location2 => "New Delhi", :location1 => "Mumbai"})
r.price = "$100"
r.save!