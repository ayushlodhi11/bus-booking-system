class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :user, except: [:login, :logout, :new, :create]

  def book
    if params["location1"].present? and params["location2"].present?
      if  params["location1"] ==  params["location2"]
        @notice = "origin and destination cannot be same" 
      else
        @rate = Rate.find_by(:location1 => params["location1"], location2: params[:location2])
      end
    end
  end

  def bookticket
    id = params[:id]
    t = Ticket.create({:rate_id => id, :user_id => @user.id})
    redirect_to "/mytickets"
    @booked = true
  end

  def mytickets
    @mytickets = Ticket.joins(:rate).where(:user_id => @user.id)
    render layout: false
  end

  def cancel
    book_id = params[:id]
    u = Ticket.find_by({:id => book_id, :user_id => @user.id})
    u.delete if u.present?
    @cancel = true
    redirect_to "/mytickets"
  end

  def contact
    render layout: false
  end

  def login
    if params["username"].present? and params["password"].present?
      user = User.find_by(username: params["username"], password: params["password"])
      if user.present?
        cookies["username"] = user.username
        page = "/book"
        redirect_to page and return
      end
      @invalid = true
    end
    render layout: "login"
  end

  def logout
    cookies.delete(:username)
    cookies.delete(:is_admin)
    redirect_to "/" and return
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
    render layout: "login"
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to "/login", notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :password)
    end

    def user
      @user = User.find_by(username: cookies["username"])
    end
end
