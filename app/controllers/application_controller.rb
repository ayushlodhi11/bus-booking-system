class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  before_filter :authorize

  $LOCATION = ["Chennai", "Mumbai", "New Delhi"]
  def authorize
      username = cookies[:username]
      if !username.present? and params["action"] != "new" and params["action"] != "create"
        redirect_to "/login" and return if params["action"] != "login" 
      elsif params["action"] == "login"
        page = "/book"
          redirect_to page and return
      end
  end
end
