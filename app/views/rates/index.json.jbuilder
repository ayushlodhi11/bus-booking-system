json.array!(@rates) do |rate|
  json.extract! rate, :id, :location1, :location2, :price
  json.url rate_url(rate, format: :json)
end
